package me.bynetherdude.autoshutdown;

import me.bynetherdude.autoshutdown.events.PlayerQuit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public final class AutoShutdown extends JavaPlugin {

    private FileConfiguration config = getConfig();

    @Override
    public void onEnable() {
        System.out.println("AutoShutdown has been activated!");
        eventRegistration();

        // Config
        config.options().copyDefaults(true);
        config.addDefault("shutdownDelayMinutes", 5);
        saveConfig();
    }

    private void eventRegistration() {
        getServer().getPluginManager().registerEvents(new PlayerQuit(), this);
    }
}
