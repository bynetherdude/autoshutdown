package me.bynetherdude.autoshutdown.events;

import me.bynetherdude.autoshutdown.AutoShutdown;
import me.bynetherdude.autoshutdown.shutdown.Shutdown;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuit implements Listener {
    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        // Check if more than one player is online at the time of the quit (quitting player counts too)
        if(AutoShutdown.getPlugin(AutoShutdown.class).getServer().getOnlinePlayers().size() == 1) {
            Shutdown shutdown = new Shutdown();
            shutdown.startScheduler();
        }
    }
}
