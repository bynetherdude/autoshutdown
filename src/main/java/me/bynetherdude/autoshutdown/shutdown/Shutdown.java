package me.bynetherdude.autoshutdown.shutdown;

import me.bynetherdude.autoshutdown.AutoShutdown;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;

public class Shutdown {

    private int delayMinutes;

    public Shutdown() {
        FileConfiguration config = AutoShutdown.getPlugin(AutoShutdown.class).getConfig();

        if(config.getInt("shutdownDelayMinutes") != 0) {
            this.delayMinutes = config.getInt("shutdownDelayMinutes");
        } else {
            this.delayMinutes = 5;
        }
    }

    /**
     * This method actually shuts down the server
     */
    public void execute() {
        ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
        String command = "stop";
        Bukkit.dispatchCommand(console, command);
    }

    /**
     * Starts a scheduler that checks the player count and shuts down the server after a given time
     */
    public void startScheduler() {
        System.out.println("Shutting down in " + delayMinutes + " minute/s!");
        Bukkit.getScheduler().runTaskLater(AutoShutdown.getPlugin(AutoShutdown.class), new Runnable() {
            @Override
            public void run() {
                if(!(AutoShutdown.getPlugin(AutoShutdown.class).getServer().getOnlinePlayers().size() > 0)) {
                    execute();
                }
            }
        }, 20*(delayMinutes*60));
    }
}
