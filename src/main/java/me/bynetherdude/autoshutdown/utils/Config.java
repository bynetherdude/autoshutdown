package me.bynetherdude.autoshutdown.utils;

import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Config {

    private final File file;
    private final YamlConfiguration config;

    public Config() {
        File configDir = new File("./plugins/AutoShutdown/");

        if(!configDir.exists()) {
            configDir.mkdirs();
        }

        this.file = new File(configDir, "config.yml");

        if(!file.exists()) {
            try {
                file.createNewFile();
                writeDefaultContent(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.config = YamlConfiguration.loadConfiguration(file);
    }

    private void writeDefaultContent(File configFile) {
        try {
            FileWriter fw = new FileWriter(configFile);
            fw.write("shutdown.delayminutes: 5");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public YamlConfiguration getConfig() {
        return config;
    }
}
